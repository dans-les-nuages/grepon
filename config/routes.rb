# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'home#index'
  devise_for :users
  resources :domains
  get '/domains/update/:id', to: 'domains#update'
  list = [%w[dns dnsrrs], %w[header headers], %w[mail mailconfigs], %w[port ports], %w[tls tlsconfigs], %w[whois whoisrecords]]
  list.each do |obj|
    get "/#{obj[0]}/:id", to: "#{obj[1]}#show"
    get "/#{obj[0]}/update/:id", to: "#{obj[1]}#update"
    get "/guide/#{obj[0]}", to: "guide##{obj[0]}"
  end
  get '/guide', to: 'guide#index'
  get '/tools', to: 'tools#index'
  get '/tools/base64', to: 'base64#index'
  post '/tools/base64/encode', to: 'base64#show'
  post '/tools/base64/decode', to: 'base64#show'
  get '/tools/csr', to: 'csr#index'
  post '/tools/csr', to: 'csr#show'
  get '/tools/dig', to: 'dig#index'
  post '/tools/dig', to: 'dig#show'
  get '/tools/dnspropagation', to: 'dnspropagation#index'
  post '/tools/dnspropagation', to: 'dnspropagation#show'
  get '/tools/password', to: 'pass#index'
  post '/tools/password', to: 'pass#show'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
