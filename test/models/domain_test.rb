# frozen_string_literal: true

require 'test_helper'

class DomainTest < ActiveSupport::TestCase
  test 'domain FQDN is set before saving' do
    d = Domain.new
    assert_not d.save, 'Saved the domain without a fqdn'
  end
end
