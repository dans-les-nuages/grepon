# frozen_string_literal: true

require 'application_system_test_case'

class TlsconfigsTest < ApplicationSystemTestCase
  setup do
    @tlsconfig = tlsconfigs(:one)
  end

  test 'visiting the index' do
    visit tlsconfigs_url
    assert_selector 'h1', text: 'Tlsconfigs'
  end

  test 'creating a Tlsconfig' do
    visit tlsconfigs_url
    click_on 'New Tlsconfig'

    fill_in 'Protocol', with: @tlsconfig.protocol
    fill_in 'Version', with: @tlsconfig.version
    click_on 'Create Tlsconfig'

    assert_text 'Tlsconfig was successfully created'
    click_on 'Back'
  end

  test 'updating a Tlsconfig' do
    visit tlsconfigs_url
    click_on 'Edit', match: :first

    fill_in 'Protocol', with: @tlsconfig.protocol
    fill_in 'Version', with: @tlsconfig.version
    click_on 'Update Tlsconfig'

    assert_text 'Tlsconfig was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Tlsconfig' do
    visit tlsconfigs_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Tlsconfig was successfully destroyed'
  end
end
