# frozen_string_literal: true

require 'application_system_test_case'

class DnsrrsTest < ApplicationSystemTestCase
  setup do
    @dnsrr = dnsrrs(:one)
  end

  test 'visiting the index' do
    visit dnsrrs_url
    assert_selector 'h1', text: 'Dnsrrs'
  end

  test 'creating a Dnsrr' do
    visit dnsrrs_url
    click_on 'New Dnsrr'

    fill_in 'Domainname', with: @dnsrr.domainname
    fill_in 'Ttl', with: @dnsrr.ttl
    fill_in 'Type', with: @dnsrr.type
    fill_in 'Value', with: @dnsrr.value
    click_on 'Create Dnsrr'

    assert_text 'Dnsrr was successfully created'
    click_on 'Back'
  end

  test 'updating a Dnsrr' do
    visit dnsrrs_url
    click_on 'Edit', match: :first

    fill_in 'Domainname', with: @dnsrr.domainname
    fill_in 'Ttl', with: @dnsrr.ttl
    fill_in 'Type', with: @dnsrr.type
    fill_in 'Value', with: @dnsrr.value
    click_on 'Update Dnsrr'

    assert_text 'Dnsrr was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Dnsrr' do
    visit dnsrrs_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Dnsrr was successfully destroyed'
  end
end
