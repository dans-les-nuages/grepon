# frozen_string_literal: true

require 'application_system_test_case'

class WhoisTest < ApplicationSystemTestCase
  setup do
    @whoi = whois(:one)
  end

  test 'visiting the index' do
    visit whois_url
    assert_selector 'h1', text: 'Whois'
  end

  test 'creating a Whoi' do
    visit whois_url
    click_on 'New Whoi'

    fill_in 'Domainname', with: @whoi.domainname
    fill_in 'Value', with: @whoi.value
    click_on 'Create Whoi'

    assert_text 'Whoi was successfully created'
    click_on 'Back'
  end

  test 'updating a Whoi' do
    visit whois_url
    click_on 'Edit', match: :first

    fill_in 'Domainname', with: @whoi.domainname
    fill_in 'Value', with: @whoi.value
    click_on 'Update Whoi'

    assert_text 'Whoi was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Whoi' do
    visit whois_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Whoi was successfully destroyed'
  end
end
