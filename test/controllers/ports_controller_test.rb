# frozen_string_literal: true

require 'test_helper'

class PortsControllerTest < ActionDispatch::IntegrationTest
  test 'should get show' do
    get ports_show_url
    assert_response :success
  end
end
