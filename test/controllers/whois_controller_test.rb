# frozen_string_literal: true

require 'test_helper'

class WhoisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @whoi = whois(:one)
  end

  test 'should get index' do
    get whois_url
    assert_response :success
  end

  test 'should get new' do
    get new_whoi_url
    assert_response :success
  end

  test 'should create whoi' do
    assert_difference('Whoi.count') do
      post whois_url, params: { whoi: { domainname: @whoi.domainname, value: @whoi.value } }
    end

    assert_redirected_to whoi_url(Whoi.last)
  end

  test 'should show whoi' do
    get whoi_url(@whoi)
    assert_response :success
  end

  test 'should get edit' do
    get edit_whoi_url(@whoi)
    assert_response :success
  end

  test 'should update whoi' do
    patch whoi_url(@whoi), params: { whoi: { domainname: @whoi.domainname, value: @whoi.value } }
    assert_redirected_to whoi_url(@whoi)
  end

  test 'should destroy whoi' do
    assert_difference('Whoi.count', -1) do
      delete whoi_url(@whoi)
    end

    assert_redirected_to whois_url
  end
end
