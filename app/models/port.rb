# frozen_string_literal: true

class Port < ApplicationRecord
  belongs_to :domain, dependent: :delete
  validates :number, numericality: { only_integer: true }, presence: true
end
