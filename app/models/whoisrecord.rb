# frozen_string_literal: true

class Whoisrecord < ApplicationRecord
  belongs_to :domain
end
