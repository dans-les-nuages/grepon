# frozen_string_literal: true

class Password < ApplicationRecord
  validates :length, length: { in: 1..2 }, numericality: { only_integer: true }
end
