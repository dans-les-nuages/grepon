# frozen_string_literal: true

# Grepon super controller
class GreponController < ApplicationController
  before_action :authenticate_user!, :set_user, :set_domain

  private

  def set_domain
    @domain = Domain.find(params[:id])
    @domain = nil if @domain.user_id != @user.id
  end

  def set_user
    @user = current_user
  end
end
