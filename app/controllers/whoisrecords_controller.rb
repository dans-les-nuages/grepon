# frozen_string_literal: true

# Whois query
class WhoisrecordsController < GreponController
  before_action :set_domain, :authenticate_user!

  # GET /whois/update/1
  def update
    wu = WhoisUpdater.new(@domain)
    wu.clean
    wu.create
    redirect_to action: 'show', id: @domain.id
  end

  # GET /whois/1
  def show
    set_whois
    set_lastupdate
  end

  private

  def set_domain
    @domain = Domain.find(params[:id])
  end

  def set_whois
    @whois = @domain.whoisrecord
  end

  def set_lastupdate
    @lastupdate = if @whois
                    @whois.updated_at.to_formatted_s(:short)
                  else
                    'Never'
                  end
  end
end
