# frozen_string_literal: true

# Encode/Decode Base64
class Base64Controller < ApplicationController
  # GET /tools/base64
  def index; end

  # POST /tools/base64/encode
  def show
    text = params[:b64en]
    b64 = params[:b64de]
    begin
      @result = Base64.strict_encode64(text) if text
      @result = Base64.strict_decode64(b64) if b64
    rescue StandardError
      @result = 'Not valid'
    end
  end
end
