# frozen_string_literal: true

# Tools page
class ToolsController < ApplicationController
  # GET /tools
  def index; end
end
