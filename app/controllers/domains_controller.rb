# frozen_string_literal: true

# Domains controller
class DomainsController < GreponController
  before_action :set_domain, only: %i[show update destroy]

  # GET /domains
  def index
    @domains = Domain.where(user_id: @user.id)
  end

  # GET /domains/1
  def show
    redirect_to domains_url, notice: "You can't see this domain." if @domain.nil?
  end

  # GET /domains/update/1
  def update
    upds = [DnsrrsUpdater, HeadersUpdater, PortsUpdater, TlsconfigUpdater, WhoisUpdater]
    upds.each do |upd|
      o = upd.new(@domain)
      o.refresh
    end
    redirect_to action: 'show', id: @domain.id
  end

  # POST /domains
  def create
    @domain = Domain.new(domain_params)
    @domain.fqdn = @domain.fqdn.downcase
    set_maindomain
    @domain.user_id = @user.id
    redirect_to '/', notice: 'Domain doesnt exist' and return if domain_inexist?
    redirect_to @domain, notice: 'Domain is already there.' and return if domain_already_present?
    redirect_to @domain, notice: 'Domain was successfully created.' and return if @domain.save

    redirect_to '/', alert: 'Error while saving domain.'
  end

  # DELETE /domains/1
  def destroy
    @domain.destroy
    respond_to do |format|
      format.html { redirect_to domains_url, notice: 'Domain was successfully destroyed.' }
    end
  end

  private

  def set_maindomain
    d = @domain.fqdn.split('.')
    count = d.count
    d = "#{d[count - 2]}.#{d[count - 1]}"
    @domain.maindomain = d
  end

  # Only allow a list of trusted parameters through.
  def domain_params
    params.require(:domain).permit(:fqdn)
  end

  def domain_inexist?
    rsv = Dnsruby::DNS.new
    begin
      rsv.getaddress(@domain.fqdn)
    rescue StandardError
      return true
    end
    false
  end

  def domain_already_present?
    dmn = Domain.where(fqdn: @domain.fqdn, user_id: @user.id)
    return true unless dmn.empty?

    false
  end
end
