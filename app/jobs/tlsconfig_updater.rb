# frozen_string_literal: true

# Update TLS configs
class TlsconfigUpdater
  def initialize(domain)
    @domain = domain
  end

  def refresh
    clean
    create
  end

  def clean
    @tls = @domain.tlsconfigs
    @tls.each(&:delete)
  end

  def create
    %w[SSLv3 TLSv1 TLSv1_1 TLSv1_2].each do |version|
      isused = true
      begin
        HTTParty.head("https://#{@domain.fqdn}", { ssl_version: version, timeout: 3 })
      rescue StandardError
        isused = false
      end
      Tlsconfig.create(domain_id: @domain.id, version: version, isused: isused)
    end
  end
end
