# frozen_string_literal: true

# Update Whois records
class WhoisUpdater
  def initialize(domain)
    @domain = domain
  end

  def refresh
    clean
    create
  end

  def clean
    @whois = @domain.whoisrecord
    @whois&.delete
  end

  def create
    begin
      who = Whois.whois(@domain.maindomain)
      who = who.to_s.force_encoding('utf-8')
    rescue StandardError
      who = 'No record found'
    end
    Whoisrecord.create(value: who, domain_id: @domain.id)
  end
end
