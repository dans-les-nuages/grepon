# frozen_string_literal: true

# Generates 5 password
class PasswordGenerator
  def self.gen(pwd)
    pwgen = PawGen.new
    pwgen.set_length!(pwd.length) if pwd.length
    pwgen.no_digits! unless pwd.numbers
    pwgen.no_uppercase! unless pwd.upper
    pwgen.include_symbols! if pwd.specials
    pass = []
    5.times do
      pass << pwgen.anglophonemic
    end
    pass
  end
end
