# frozen_string_literal: true

# Request DNS from multiple servers all over the world
class DnsPropagation
  def initialize(fqdn)
    @fqdn = fqdn
    @dnslist = {
      'Online' => ['🇫🇷', 'France', '163.172.107.158'],
      'OpenDNS' => ['🇺🇸', 'USA', '208.67.222.220'],
      'Teknet Yazlim' => ['🇹🇷', 'Turkey', '31.7.37.37'],
      'Hetzner' => ['🇿🇦', 'South Africa', '197.189.234.82'],
      'Pyton' => ['🇳🇱', 'Netherland', '193.58.204.59'],
      'Railwire' => ['🇮🇳', 'India', '112.133.219.34'],
      'Google' => ['🇺🇸', 'USA', '8.8.8.8'],
      'KT Corp' => ['🇰🇷', 'South Corea', '168.126.63.1'],
      'AT&T' => ['🇺🇸', 'USA', '12.121.117.201'],
      'Cloudflare' => ['🇦🇺', 'Australia', '1.1.1.1'],
      'Indigo' => ['🇮🇪', 'Ireland', '194.125.133.10'],
      'ServiHosting' => ['🇪🇸', 'Spain', '84.236.142.130'],
      'SkyDNS' => ['🇷🇺', 'Russia', '195.46.39.39'],
      'Verizon' => ['🇬🇧', 'UK', '158.43.128.1'],
      'IPP' => ['🇨🇳', 'China', '114.114.115.119'],
      'Video Elephant' => ['🇧🇩', 'Bangladesh', '137.59.155.14'],
      'Sprint' => ['🇺🇸', 'USA', '204.117.214.10']
    }
  end

  def get
    rsp = {}
    @dnslist.each do |server|
      addr = begin
        Resolv::DNS.new(nameserver: server[1][2], timeouts: 1).getaddress(@fqdn)
      rescue StandardError
        'No response'
      end
      rsp["#{server[1][0]} #{server[0]}"] = addr
    end
    rsp
  end
end
