# frozen_string_literal: true

class AddHeadersscoreToDomain < ActiveRecord::Migration[6.1]
  def change
    add_column :domains, :headers_score, :float
  end
end
