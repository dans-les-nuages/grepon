# frozen_string_literal: true

class ChangeToHasOne < ActiveRecord::Migration[6.1]
  def change
    remove_column :dnsrrs, :domain_id, :domain
    remove_column :whois, :domain_id, :domain
    remove_column :headers, :domain_id, :domain
    remove_column :tlsconfigs, :domain_id, :domain
    add_column :dnsrrs, :domain, :belongs_to, index: { unique: true }, foreign_key: true
    add_column :whois, :domain, :belongs_to, index: { unique: true }, foreign_key: true
    add_column :headers, :domain, :belongs_to, index: { unique: true }, foreign_key: true
    add_column :tlsconfigs, :domain, :belongs_to, index: { unique: true }, foreign_key: true
  end
end
