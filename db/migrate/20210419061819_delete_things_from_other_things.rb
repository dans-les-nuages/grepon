# frozen_string_literal: true

class DeleteThingsFromOtherThings < ActiveRecord::Migration[6.1]
  def change
    remove_column :dnsrrs, :domainname, :string
    remove_column :dnsrrs, :ttl, :integer
    remove_column :dnsrrs, :score, :integer
    remove_column :dnsrrs, :dnsserver, :string
    remove_column :domains, :headers_score, :integer
    remove_column :whoisrecords, :domainname, :string
    remove_column :whoisrecords, :score, :integer
    drop_table :mailconfigs do |t|
      t.string :spf
      t.string :dkim
      t.string :dmarc
      t.integer :score
      t.belongs_to :domain
    end
  end
end
