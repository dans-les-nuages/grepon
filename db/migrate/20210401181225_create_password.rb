# frozen_string_literal: true

class CreatePassword < ActiveRecord::Migration[6.1]
  def change
    create_table :passwords do |t|
      t.integer :length
      t.boolean :lower
      t.boolean :upper
      t.boolean :numbers
      t.boolean :specials

      t.timestamps
    end
  end
end
