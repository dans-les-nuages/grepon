# frozen_string_literal: true

class CreateWhois < ActiveRecord::Migration[6.1]
  def change
    create_table :whois do |t|
      t.string :domainname
      t.text :value

      t.timestamps
    end
  end
end
