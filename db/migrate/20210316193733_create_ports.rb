# frozen_string_literal: true

class CreatePorts < ActiveRecord::Migration[6.1]
  def change
    create_table :ports do |t|
      t.integer :number
      t.string :protocol
      t.boolean :opened

      t.timestamps
    end
  end
end
