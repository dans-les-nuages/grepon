# frozen_string_literal: true

class AddScoresToHeaders < ActiveRecord::Migration[6.1]
  def change
    add_column :headers, :csp_score, :integer
    add_column :headers, :hsts_score, :integer
    add_column :headers, :https_score, :integer
    add_column :headers, :referrer_score, :integer
    add_column :headers, :server_score, :integer
    add_column :headers, :xcto_score, :integer
    add_column :headers, :xfo_score, :integer
    add_column :headers, :xxss_score, :integer
  end
end
