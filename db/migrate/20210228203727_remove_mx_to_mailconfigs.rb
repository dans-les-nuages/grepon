# frozen_string_literal: true

class RemoveMxToMailconfigs < ActiveRecord::Migration[6.1]
  def change
    remove_column :mailconfigs, :mx, :string
    remove_column :mailconfigs, :spf, :string
    remove_column :mailconfigs, :dkim, :string
    remove_column :mailconfigs, :dmarc, :string
  end
end
