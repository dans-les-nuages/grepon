# frozen_string_literal: true

class AddScoreToDomains < ActiveRecord::Migration[6.1]
  def change
    add_column :domains, :score, :float
  end
end
