# frozen_string_literal: true

class CreateHeaders < ActiveRecord::Migration[6.1]
  def change
    create_table :headers do |t|
      t.string :type
      t.string :value
      t.boolean :secure

      t.timestamps
    end
  end
end
