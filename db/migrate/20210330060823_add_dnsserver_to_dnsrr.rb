# frozen_string_literal: true

class AddDnsserverToDnsrr < ActiveRecord::Migration[6.1]
  def change
    add_column :dnsrrs, :dnsserver, :string
  end
end
