# frozen_string_literal: true

class AddUseridToDomain < ActiveRecord::Migration[6.1]
  def change
    add_column :domains, :user_id, :user
  end
end
