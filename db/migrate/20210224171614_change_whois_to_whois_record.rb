# frozen_string_literal: true

class ChangeWhoisToWhoisRecord < ActiveRecord::Migration[6.1]
  def change
    rename_table :whois, :whoisrecords
  end
end
