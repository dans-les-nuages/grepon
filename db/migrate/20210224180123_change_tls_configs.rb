# frozen_string_literal: true

class ChangeTlsConfigs < ActiveRecord::Migration[6.1]
  def change
    remove_column :tlsconfigs, :protocol, :string
    remove_column :tlsconfigs, :version, :string
    add_column :tlsconfigs, :sslv3, :boolean
    add_column :tlsconfigs, :tlsv10, :boolean
    add_column :tlsconfigs, :tlsv11, :boolean
    add_column :tlsconfigs, :tlsv12, :boolean
  end
end
