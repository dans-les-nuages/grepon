# frozen_string_literal: true

class ChangeHeadersScore < ActiveRecord::Migration[6.1]
  def change
    remove_column :headers, :score, :integer
    remove_column :headers, :type, :string
    add_column :headers, :issecure, :boolean
  end
end
