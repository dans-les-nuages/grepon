# frozen_string_literal: true

class AddScoreToTlsconfigs < ActiveRecord::Migration[6.1]
  def change
    add_column :tlsconfigs, :sslv3_score, :integer
    add_column :tlsconfigs, :tlsv10_score, :integer
    add_column :tlsconfigs, :tlsv11_score, :integer
    add_column :tlsconfigs, :tlsv12_score, :integer
  end
end
