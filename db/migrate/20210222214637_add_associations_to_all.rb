# frozen_string_literal: true

class AddAssociationsToAll < ActiveRecord::Migration[6.1]
  def change
    add_column :dnsrrs, :domain_id, :domain
    add_column :whois, :domain_id, :domain
    add_column :headers, :domain_id, :domain
    add_column :tlsconfigs, :domain_id, :domain
  end
end
