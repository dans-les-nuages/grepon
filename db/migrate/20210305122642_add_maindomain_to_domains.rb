# frozen_string_literal: true

class AddMaindomainToDomains < ActiveRecord::Migration[6.1]
  def change
    add_column :domains, :maindomain, :string
  end
end
