# frozen_string_literal: true

class AddTypeToHeaders < ActiveRecord::Migration[6.1]
  def change
    add_column :headers, :type, :string
  end
end
