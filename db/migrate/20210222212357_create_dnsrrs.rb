# frozen_string_literal: true

class CreateDnsrrs < ActiveRecord::Migration[6.1]
  def change
    create_table :dnsrrs do |t|
      t.string :domainname
      t.string :type
      t.integer :ttl
      t.string :value

      t.timestamps
    end
  end
end
